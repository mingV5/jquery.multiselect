/**
 * Created by mingzi on 2016/5/26.
 * radio、checkbox、switchbox jquery插件；渲染插件
 */
if (typeof jQuery === 'undefined') {
	throw new Error('jquery-plugin requires jQuery');
}

;(function($,window,document,undefined) {
	$.fn.jqRadio = function (options) {
		var JRadio = new jRadio(this, options);
		return JRadio.init();
	};

	$.fn.jqCheckBox = function (options) {
		var JCheckBox = new jRadio(this, options);
		return JCheckBox.init();
	};

	$.fn.jqSwitch = function (options) {
        var JSwitch = new jSwitch(this,options);
        return JSwitch.init();
	}

	$.fn.rlPluginDefault = {
		width:'auto',
        double: false,
		defaultClass:'rl-radio',
		checkedClass: 'rl-radioon',
        pointClass: 'switchon',
        opentitle:'提醒',
        closetitle:'不提醒',
		onClass:'rl-on',
        offClass:'rl-off',
		actionClass:'on',
		onClick: function (target) {}
	};

	var jSwitch = function (el,opt) {
		this.$element = el,
        this.options = $.extend({},$.fn.rlPluginDefault,opt);
    };

    jSwitch.prototype ={
        init : function (){
            var that = this, $el = that.$element;
            return $el.each(function (index, el) {
                var $this = $(el),
                    inputs = $el.find('input');
                that.rendu($this);

            });
        },
        rendu : function(el){
            var $el = el,
                that = this,
                inputs = $el.find('input'),
                thisText = $el.text(),
                name = inputs.attr('name') ? inputs.attr('name') : '',
                value = inputs.val(),
                checked = inputs.prop('checked'),
                tempHtml = that.tmpHtml(checked),
                thisStatus = inputs.prop('disabled');
            if( $el.find('em').index()<0){
                inputs.before(tempHtml).hide();
            }
            $el.find('em').attr('data-name', name).attr('data-value', value);
            if(that.options.double){
                $el.addClass('rl-sdouble');
                var spanW = that.options.width;
                $el.find('i').width(that.options.width/2);
                $el.find('i.'+that.options.onClass).css('left',that.options.width/2);
                $el.find('em').width(that.options.width);
                $el.find('span').width(that.options.width/2);
                if(checked){
                    $el.find('span').css('left',that.options.width/2);
                }else{
                    $el.find('span').css('left',0);
                }
			}else{
            	var spanW = that.options.width- $el.find('em').width();
                $el.find('i').width(spanW);
			}
            if(thisStatus){
                $el.addClass('rl-disabled');
                that.bind($el,'bind');
            }else {
                that.bind($el, 'click');
            }
        },
        tmpHtml : function(state){
            var that = this;
            var html =[];
            if(state){
                html.push('<em class="'+that.options.defaultClass+' '+that.options.checkedClass+'"><span class="'+that.options.pointClass+'"></span></em>');
                html.push('<i class="'+that.options.offClass+' ">'+that.options.closetitle+'</i>');
                html.push('<i class="'+that.options.onClass+' '+that.options.actionClass+'">'+that.options.opentitle+'</i>');
            }else{
                html.push('<em class="'+that.options.defaultClass+'"><span class="'+that.options.pointClass+'"></span></em>');
                html.push('<i class="'+that.options.offClass+' '+that.options.actionClass+'">'+that.options.closetitle+'</i>');
                html.push('<i class="'+that.options.onClass+'">'+that.options.opentitle+'</i>');
            }
            return html.join(' ');
        },
        bind : function(el, type){
            var $el = el,that = this;
            $el.find('i').unbind(type);
            $el.find('i').bind(type,function (event) {
                event.preventDefault(); //取消事件的默认动作
                event.stopPropagation();//不再派发事件; 取消事件在
				var $el=$(event.target).parent();
                that.radioClick($el);
                if( typeof that.options.onClick == 'function'){
                    that.options.onClick($el);
                }
            });
            $el.find('span').unbind(type);
            $el.find('span').bind(type,function (event) {
                event.preventDefault(); //取消事件的默认动作
                event.stopPropagation();//不再派发事件; 取消事件在
                var $el=$(event.target).parent().parent();
                that.radioClick($el);
                if( typeof that.options.onClick == 'function'){
                    that.options.onClick($el);
                }
            });
            $el.unbind(type);
            $el.bind(type, function(event){
                event.preventDefault();
                event.stopPropagation();
                var $target = $(event.target);//事件属性可返回事件的目标节点（触发该事件的节点），如生成事件的元素、文档或窗口。
                that.radioClick($target);
                if( typeof that.options.onClick == 'function'){
                    that.options.onClick($target);
                }
            });
            $el.find('em').unbind(type);
            $el.find('em').bind(type, function(event){
                event.preventDefault(); //取消事件的默认动作
                event.stopPropagation();//不再派发事件; 取消事件在
                var $target = $(event.target).parent();
                that.radioClick($target);
                if( typeof that.options.onClick == 'function'){
                    that.options.onClick($target);
                }
            });
        },
        radioClick : function (el) {
            var $el = el,
                that = this,
                className = $el.attr('class'),
                type = $el.find('input').attr('type'),
                name = $el.find('em').attr('data-name');
                var thisChecked = $el.find('input[type="'+type+'"][name="'+name+'"]').attr('checked');
                if($el.hasClass('rl-disabled')) return;
                if(thisChecked == 'checked'){
                	$el.find('i.'+that.options.onClass).removeClass('on');
                    $el.find('i.'+that.options.offClass).addClass('on');
                    $el.find('input[type="'+type+'"][name="'+name+'"]').removeAttr('checked');
                    $el.find('em').removeClass(that.options.checkedClass);
                    if(that.options.double){
                        $el.find('span').css('left',0);
					}
                }else{
                    $el.find('i.'+that.options.offClass).removeClass('on');
                    $el.find('i.'+that.options.onClass).addClass('on');
                    $el.find('input[type="'+type+'"][name="'+name+'"]').attr('checked','checked');
                    $el.find('em').addClass(that.options.checkedClass);
                    if(that.options.double){
                        $el.find('span').css('left',that.options.width/2);
                    }
            }
        }
    };

	var jRadio = function(el, opt){
		this.$element = el,
		this.options = $.extend({},$.fn.rlPluginDefault,opt);
	};
	jRadio.prototype ={
		init : function (){
			var that = this, $el = that.$element;
			return $el.each(function (index, el) {
				var $this = $(el);
				that.rendu($this);
			});
		},
		rendu : function(el) {
			var $el = el,
					that = this,
					inputs = $el.find('input'),
					thisText = $el.text(),
					name = inputs.attr('name') ? inputs.attr('name') : '',
					value = inputs.val(),
					checked = inputs.prop('checked'),
					tempHtml = that.tmpHtml(checked),
                thisStatus = inputs.prop('disabled');
			inputs.before(tempHtml).hide();
			$el.find('em').attr('data-name', name).attr('data-value', value);
            if(thisStatus){
                $el.addClass('rl-disabled');
                that.bind($el,'bind');
            }else {
                that.bind($el, 'click');
            }
		},
		tmpHtml : function(state){
			var that = this;
			var html =[];
			if(state){
				html.push('<em class="'+that.options.defaultClass+' '+that.options.checkedClass+'"></em>');
			}else{
				html.push('<em class="'+that.options.defaultClass+'"></em>');
			}
			return html.join(' ');
		},
		bind : function(el, type){
			var $el = el,that = this;
            $el.unbind(type);
			$el.bind(type, function(event){
				event.preventDefault();
				event.stopPropagation();
				var $target = $(event.target);
				that.radioClick($target);
				if( typeof that.options.onClick == 'function'){
					that.options.onClick($target);
				}
			});
            $el.find('em').unbind(type);
			$el.find('em').bind(type, function(event){
				event.preventDefault();
				event.stopPropagation();
				var $target = $(event.target).parent();
				that.radioClick($target);
				if( typeof that.options.onClick == 'function'){
					that.options.onClick($target);
				}
			});
		},
		radioClick : function (el) {
			var $el = el,
					that = this,
					className = $el.attr('class'),
					classReplace = className.replace(' ', '.'),
					type = $el.find('input').attr('type'),
					name = $el.find('em').attr('data-name');
            if($el.hasClass('rl-disabled')) return;
			if(type == 'radio'){
				$('.'+classReplace).find('em[data-name="'+name+'"]').removeClass(that.options.checkedClass);
				$('.'+classReplace).find('input[type="'+type+'"][name="'+name+'"]').removeAttr('checked');
				$el.find('em').addClass(that.options.checkedClass);
				$el.find('input[type="'+type+'"][name="'+name+'"]').prop('checked',true);
			}else if(type == 'checkbox'){
				var thisChecked = $el.find('input[type="'+type+'"][name="'+name+'"]').attr('checked');
				if(thisChecked == 'checked'){
					$el.find('input[type="'+type+'"][name="'+name+'"]').removeAttr('checked');
					$el.find('em[data-name="'+name+'"]').removeClass(that.options.checkedClass);
				}else{
					$el.find('input[type="'+type+'"][name="'+name+'"]').attr('checked','checked');
					$el.find('em').addClass(that.options.checkedClass);
				}
			}
		}
	};



})(jQuery,window,document);